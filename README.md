# soal-shift-sisop-modul-3-F04-2022
anggota kelompok:
sayid ziyad ibrahim alaydrus 5025201147.
rafiqi rachmat.
david ficher.

## soal no. 1
a. mengunzip file quote dan music 
```
void* FungsiA(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){ //thread untuk clear layar
        int status;
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"mkdir", "-p", "./quote", NULL};
   		    execv("/bin/mkdir", argv);
        }else{
            while(wait(&status) > 0);
            int status2;
            pid_t child2 = fork();
            if(child2 == 0){
                char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", 
                "-O","quote.zip"};
   		        execv("/bin/wget", argv);
            }else{
                while(wait(&status2) > 0);
                int status3;
                pid_t child3 = fork();
                if(child3 == 0){
                    char *argv[] = {"unzip", "-q", "./quote.zip", "-d", "./quote", NULL};
                    execv("/bin/unzip", argv);
``` 
setelanh selesai mengunzip quote lanjut ke music dengan algoritma yang sama

b.) mendecode file

```
void decode(char str[]){
    int check = 0;
    char temp[4];
    char arr[1000]; 
    int i = 0, p = 0;

    for(i = 0; str[i] != '\0'; i++) {
        int k;
        for(k = 0 ; k < 64 && base64[k] != str[i]; k++);
        temp[check++] = k;
        if(check == 4) {
            arr[p++] = (temp[0] << 2) + (temp[1] >> 4);
            if(temp[2] != 64)
                arr[p++] = (temp[1] << 4) + (temp[2] >> 2);
            if(temp[3] != 64)
                arr[p++] = (temp[2] << 6) + temp[3];
            check = 0;
        }
```

c.) masukkan file yang sudah ke decode ke folder hasil
```
int status = 0;
   	if(fork() == 0){
        char *argv[] = {"mkdir", "-p", "./hasil", NULL};
   		execv("/bin/mkdir", argv);
    }else{
        while(wait(&status) > 0);
        int status2;
        if(fork() == 0){
            char *argv[] = {"find", "./quote", "-name", "quote.txt", "-exec", "mv", "-t","./hasil", "{}","+",NULL };
            execv("/bin/find", argv);
        }else{
            while(wait(&status2) > 0);
            int status3;
            if(fork() == 0){
                char *argv[] = {"find", "./music", "-name", "music.txt", "-exec", "mv", "-t","./hasil", "{}","+",NULL };
                execv("/bin/find", argv);   
```
d.) lalu zip dan  berikan passwors sesuai ketentuan
```
void zipping(){
  pid_t child_id;
  child_id = fork();
  if(child_id != 0){
              char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestziyad", "hasil", NULL}; 
          execv("/usr/bin/zip", args);
  }
```

d.) tambahkan file no.txt
```
else if(pthread_equal(id, tid[1])){
        FILE *fptr = fopen("./hasil/no.txt","w");
        fprintf(fptr,"No\n");
        fclose(fptr);
```
hasil :
![1.a](/uploads/aafaae046bea16f891c3a0fb5445ccd6/messageImage_1650204742096.jpg)

![1.c](/uploads/1d8380c0986d2539e2f2f8290b0efbe6/messageImage_1650204763344.jpg)

![1.e](/uploads/ede51fa94695e3e394eba74bc0ece936/messageImage_1650204785415.jpg)



`
