#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include<dirent.h>
#include<pthread.h>

pthread_t tid[2];

void* FungsiA(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){ //thread untuk clear layar
        int status;
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"mkdir", "-p", "./quote", NULL};
   		    execv("/bin/mkdir", argv);
        }else{
            while(wait(&status) > 0);
            int status2;
            pid_t child2 = fork();
            if(child2 == 0){
                char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", 
                "-O","quote.zip"};
   		        execv("/bin/wget", argv);
            }else{
                while(wait(&status2) > 0);
                int status3;
                pid_t child3 = fork();
                if(child3 == 0){
                    char *argv[] = {"unzip", "-q", "./quote.zip", "-d", "./quote", NULL};
                    execv("/bin/unzip", argv);
                }
                while(wait(&status3) > 0);
            }
        }
    }else if(pthread_equal(id, tid[1])){ // thread menampilkan counter
        int status;
        pid_t child = fork();
        if(child == 0){
            char *argv[] = {"mkdir", "-p", "./music", NULL};
   		    execv("/bin/mkdir", argv);
        }else{
            while(wait(&status) > 0);
            int status2;
            pid_t child2 = fork();
            if(child2 == 0){
                char *argv[] = {"wget", "-q", "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", 
                "-O","music.zip"};
   		        execv("/bin/wget", argv);
            }else{
                while(wait(&status2) > 0);
                int status3;
                pid_t child3 = fork();
                if(child3 == 0){
                    char *argv[] = {"unzip", "-q", "./music.zip", "-d", "./music", NULL};
                    execv("/bin/unzip", argv);
                }
                while(wait(&status3) > 0);
            }
        }
    }
}

void thread(){
	pid_t child_id;
 	child_id = fork();
	int status;
  	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}

	if (child_id == 0) {	
		sleep(10);
		int err;
		unsigned long i =0;
		while(i<2) 
		{
			err=pthread_create(&(tid[i]),NULL,&FungsiA, NULL ); //create thread
			if(err!=0) //cek error
			{
				printf("can't create thread : [%s]",strerror(err));
			}
			else
			{
				printf("\n thread created \n");
			}
			i++;
		}
		pthread_join(tid[0],NULL);
		pthread_join(tid[1],NULL);
	} 

}

char base64[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

void decode(char str[]){
    int check = 0;
    char temp[4];
    char arr[1000]; 
    int i = 0, p = 0;

    for(i = 0; str[i] != '\0'; i++) {
        int k;
        for(k = 0 ; k < 64 && base64[k] != str[i]; k++);
        temp[check++] = k;
        if(check == 4) {
            arr[p++] = (temp[0] << 2) + (temp[1] >> 4);
            if(temp[2] != 64)
                arr[p++] = (temp[1] << 4) + (temp[2] >> 2);
            if(temp[3] != 64)
                arr[p++] = (temp[2] << 6) + temp[3];
            check = 0;
        }
    }
    arr[p] = '\0';
    strcpy(str, arr);
}

void* FungsiB(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){ //thread untuk clear layar
        char path[] = "./quote/";
        DIR *dir = opendir(path);
        struct dirent *dt;
        FILE *fpoinb;
        fpoinb = fopen("./quote/quote.txt","a");
        if(fpoinb == NULL){
            printf("ERROR!\n");
            exit(1);
        }
        while ((dt = readdir(dir)) != NULL){
            if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "quote.txt") != 0){
                char filepath[] = "./quote/";
                strcat(filepath, dt->d_name);
                FILE *text = fopen(filepath, "r");
                if(text == NULL){
                    printf("ERROR!\n");
                    continue;
                }
                char str[1000];
                fgets(str, sizeof(str),text);
                fclose(text);
                long len = strlen(str);
                decode(str);
                fprintf(fpoinb, "%s\n",str);
            }
        }
        fclose(fpoinb);
        closedir(dir);
    }else if(pthread_equal(id, tid[1])){ // thread menampilkan counter
        char path[] = "./music/";
        DIR *dir = opendir(path);
        struct dirent *dt;
        FILE *fpoinb;
        fpoinb = fopen("./music/music.txt","a");
        if(fpoinb == NULL){
            printf("ERROR!\n");
            exit(1);
        }
        while ((dt = readdir(dir)) != NULL){
            if(strcmp(dt->d_name,".") != 0 && strcmp(dt->d_name, "..") != 0 && strcmp(dt->d_name, "music.txt") != 0){
                char filepath[] = "./music/";
                strcat(filepath, dt->d_name);
                FILE *text = fopen(filepath, "r");
                if(text == NULL){
                    printf("ERROR!\n");
                    continue;
                }
                char str[1000];
                fgets(str, sizeof(str),text);
                fclose(text);
                long len = strlen(str);
                decode(str);
                fprintf(fpoinb, "%s\n",str);
            }
        }
        fclose(fpoinb);
        closedir(dir);
    }
}


void poinB(){
    int err;
    for(int i=0;i<2;i++){ // loop sejumlah thread
        err = pthread_create(&(tid[i]),NULL,&FungsiB, NULL);
        if(err!=0){ //cek error
            printf("\n can't create decode [%s]",strerror(err));
        }else  printf("create decode success \n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}

void PoinC(){
    int status = 0;
   	if(fork() == 0){
        char *argv[] = {"mkdir", "-p", "./hasil", NULL};
   		execv("/bin/mkdir", argv);
    }else{
        while(wait(&status) > 0);
        int status2;
        if(fork() == 0){
            char *argv[] = {"find", "./quote", "-name", "quote.txt", "-exec", "mv", "-t","./hasil", "{}","+",NULL };
            execv("/bin/find", argv);
        }else{
            while(wait(&status2) > 0);
            int status3;
            if(fork() == 0){
                char *argv[] = {"find", "./music", "-name", "music.txt", "-exec", "mv", "-t","./hasil", "{}","+",NULL };
                execv("/bin/find", argv);   
            }
            while(wait(&status3)>0);
        }
    }
}

void zipping(){
  pid_t child_id;
  child_id = fork();
  if(child_id != 0){
              char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestziyad", "hasil", NULL}; 
          execv("/usr/bin/zip", args);
  }

}

void* FungsiE(void *arg){
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])){
        int status;
        if(fork() == 0){
            char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestziyad", "hasil", NULL}; 
          execv("/usr/bin/zip", args);
        }
        while(wait(&status) > 0);
    }else if(pthread_equal(id, tid[1])){
        FILE *fptr = fopen("./hasil/no.txt","w");
        fprintf(fptr,"No\n");
        fclose(fptr);
    }
}

void PoinE(){
    int err;
    for(int i=0;i<2;i++){
        err = pthread_create(&(tid[i]),NULL,&FungsiE, NULL);
        if(err!=0){
            printf("can't create no.txt [%s]",strerror(err));
        }else  printf("create no.txt success\n");
    }
    pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
}


int main(void)
{
	thread();
	poinB();
	PoinC();
	zipping();
	PoinE();
}
